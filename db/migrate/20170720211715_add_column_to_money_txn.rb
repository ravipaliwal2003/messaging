class AddColumnToMoneyTxn < ActiveRecord::Migration
  def change
  	add_column :money_txns, :from, :integer
  	add_column :money_txns, :to, :integer
  end
end
