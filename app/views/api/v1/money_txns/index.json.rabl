if @user.nil?
	node(:error_msg) {"unauthorised"}
elsif !@money_txns.empty?
	collection @money_txns
	attributes  :currency, :amt, :received_from, :paid_to
else
	node(:error_msg) {"not found"}
end
