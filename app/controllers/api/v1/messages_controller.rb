module Api
	module V1
		class MessagesController <ApplicationController
			before_action :check_access_token
			
			def index
				if !@user.nil?
					@messages = Message.where('msg_receiver = ? OR user_id = ?', @user.id, @user.id)
				end
			end

			def show
				if !@user.nil?
					@message = Message.find(params[:id])
				end
			end
					
			private

			def check_access_token
				if params[:access_token]
					@user = User.find_by_access_token(params[:access_token])
				end
				return @user
			end
		end
	end
end