class CreateMoneyTxns < ActiveRecord::Migration
  def change
    create_table :money_txns do |t|
    	t.string :currency
    	t.string :amt
    	t.integer :message_id
    t.timestamps null: false
    end
  end
end
