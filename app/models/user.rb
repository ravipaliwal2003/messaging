class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :messages

  validates_presence_of :first_name, :last_name, :dob
  
  before_create :generate_access_token

  private

  def generate_access_token
  	counter = 0
    while counter == 0
      self.access_token = SecureRandom.hex
      if User.pluck(:access_token).include?(self.access_token)
        counter = 0
      else
        counter = 1
      end 
    end
  end
  
end
