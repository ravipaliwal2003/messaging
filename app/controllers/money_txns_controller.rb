class MoneyTxnsController < ApplicationController
	before_action :authenticate_user!

	def create
		@money_txn = MoneyTxn.new(money_txn_params)
		if @money_txn.save
			redirect_to message_path(@money_txn.message_id), notice: "Transfer Txn successfully added"
		else
			redirect_to message_path(@money_txn.message_id), notice: "Transfer Txn could not be added"
		end
	end

	def destroy
		@money_txn = MoneyTxn.find(params[:money_txn_id])
		@money_txn.destroy
		redirect_to message_path(@money_txn.message_id), notice: "Transfer Txn Successfully deleted"
	end

	def index
		@received_money_txns = MoneyTxn.where('paid_to = ?', current_user.id)
		@paid_money_txns = MoneyTxn.where('received_from = ?', current_user.id)
	end

	private

	def money_txn_params
		params[:money_txn].permit(:currency, :amt, :message_id, :received_from, :paid_to)
	end

end
