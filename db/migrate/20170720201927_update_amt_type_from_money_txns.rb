class UpdateAmtTypeFromMoneyTxns < ActiveRecord::Migration
  def change
  	change_column :money_txns, :amt, :float
  end
end
