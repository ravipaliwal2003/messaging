module Api
	module V1
		class MoneyTxnsController < ApplicationController
			before_action :check_access_token
			
			def index
				if !@user.nil?
					@money_txns = MoneyTxn.where('paid_to = ? OR received_from = ?', @user.id, @user.id)
				end
			end

			private
			
			def check_access_token
				if params[:access_token]
					@user = User.find_by_access_token(params[:access_token])
				end
				return @user
			end
		end
	end
end