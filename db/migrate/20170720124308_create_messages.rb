class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
    	t.datetime :msg_date_time
    	t.text :message
    	t.integer :msg_receiver
    	t.boolean :msg_status
    	t.boolean :sent_money
      t.timestamps null: false
    end
  end
end
