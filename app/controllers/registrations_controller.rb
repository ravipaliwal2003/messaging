class RegistrationsController < Devise::RegistrationsController
	def edit
		@user = User.find(current_user.id)
	end

	def update
		@user = User.find(current_user.id)
		if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
    		params[:user].delete(:password)
    		params[:user].delete(:password_confirmation)
		end
		
		if @user.update_attributes(sign_up_params)
			redirect_to new_user_session_path, notice: "User Details Updated Successfully"
		else
			render action: "edit"
		end
	end

	private

	def sign_up_params
		params[:user].permit(:first_name,:last_name,:dob,:email,:password,:confirm_password)		
	end

end