class MessagesController < ApplicationController
	before_action :authenticate_user!
	
	def new
		@message = Message.new
	end

	def create
		@message = Message.new(message_params)
		@message.user_id = current_user.id
		@message.msg_status = false
		if @message.save
			if @message.sent_money == true
				redirect_to message_path(@message.id), notice: "Message created successfully, add payments"
			else
				redirect_to messages_path, notice: "Message sent successfully"
			end
		else
			render action: "new", notice: "Message could not be sent, please try again"
		end
	end

	def show
		@message = Message.find(params[:id])
		if @message.msg_receiver == current_user.id
			@message.update_attributes(msg_status: true)
		end
		@money_txn = MoneyTxn.new
	end

	def index
		@received_messages = Message.where('msg_receiver = ?', current_user.id)
		@sent_messages = current_user.messages
	end
	
	def destroy
		@message = Message.find(params[:id])
		if @message.msg_status == true
			redirect_to messages_path, notice: "Message already read, can't be deleted"
		else
			@message.destroy
			redirect_to messages_path, notice: "Message deleted successfully"
		end
	end

	private
		
	def message_params 
		params[:message].permit(:msg_date_time,:subject,:message,:msg_receiver,:msg_status,:sent_money,:user_id)
	end
end
