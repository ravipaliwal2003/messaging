class Message < ActiveRecord::Base
	has_many :money_txns, dependent: :destroy
	belongs_to :user

	validates_presence_of :msg_date_time, :subject, :message, :msg_receiver, :user_id
end
