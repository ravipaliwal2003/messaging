if @user.nil?
	node(:error_msg) {"unauthorised"}
elsif !@messages.empty?

	collection @messages
	attributes  :msg_date_time, :subject, :message, :msg_status, :sent_money
	attributes :msg_receiver => :to, :user_id => :from
else
	node(:error_msg) {"not found"}
end
