class EditColumnNameFromAndToInMoneyTxnTable < ActiveRecord::Migration
  def change
  	rename_column :money_txns, :from, :received_from
  	rename_column :money_txns, :to, :paid_to
  end
end
