if @user.nil?
	node(:error_msg) {"unauthorised"}
elsif !@message.nil?
	object @message
	attributes  :msg_date_time, :msg_receiver, :subject, :message, :msg_status, :sent_money, :user_id

	child :money_txns do
		attributes :currency, :amt, :from, :to
	end
else
	node(:error_msg) {"not found"}
end
